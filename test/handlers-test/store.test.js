const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect
const co = require('co')
const dotenv = require('dotenv')

dotenv.config()

describe("Store system routes handler", function () {
	const store = require('./../../lib/handlers/store')

	let req
	let res
	let json

  req = res = next = {}

  describe("[GET /api/store] list", function () {
    it("should respond with res.json()", function (done) {
      json = res.json = sinon.spy()

      co.wrap(store.list)(req, res, next).then(res => {
      	expect(json.calledOnce).to.equal(true)
      	done()
      })
      .catch(e => done(e))
    })     
  })
})