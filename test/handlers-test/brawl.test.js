const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect
const co = require('co')
const dotenv = require('dotenv')

dotenv.config()

describe("Brawl system routes handler", function() {
  const brawl = require('./../../lib/handlers/brawl')

  let req
  let res
  let json

  req = res = next = {}

  describe("[GET /api/brawl/:enemy] resolve", function() {
    it("should resolve the brawl between api client user and the given enemy. Responds with res.json() then", function(done) {
      json = res.json = sinon.spy()

      co.wrap(brawl.brawl)(req, res, next).then(res => {
        expect(json.calledOnce).to.equal(true)
        done()
      })
      .catch(e => done(e))
    })     
  })
})