const sinon = require('sinon')
require('sinon-as-promised')
require('sinon-mongoose')
const mongoose = require('mongoose')
const chai = require('chai')
const expect = chai.expect
const co = require('co')
const dotenv = require('dotenv')

dotenv.config()

describe("User system routes handler", function () {
  const user = require('./../../lib/handlers/user')
  require('./../../lib/models/user')

  let User = mongoose.model('User')
  let mockUser
  let req
  let res
  let next
  let json

  req = res = {}
  next = sinon.spy()

  beforeEach(function () {
    req = res = {}
    next = sinon.spy()
    mockUser = sinon.mock(User)
  })

  afterEach(function () {
    mockUser.restore()
  })

  describe("[GET /api/users] list", function () {
    
    it("should get all users and respond with res.json()", function (done) {
      
      mockUser.expects('find').chain('select').resolves([])
      json = res.json = sinon.spy()
      
      co.wrap(user.list)(req, res, next).then(() => {
        mockUser.verify()
        expect(json.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    })
  })

  describe("[GET /api/users/:id] show", function () {
    
    it("should get the user and respond with res.json()", function (done) {
      
      req.user = { id: 0 }
      mockUser.expects('findById').chain('select').resolves({})
      json = res.json = sinon.spy()
      
      co.wrap(user.show)(req, res, next).then(() => {
        mockUser.verify()
        expect(json.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    })
    
    it("should try getting the user and respond with next if no user found", function (done) {
      
      req.user = { id: 0 }
      mockUser.expects('findById').chain('select').resolves(null)
      
      co.wrap(user.show)(req, res, next).then(() => {
        mockUser.verify()
        expect(next.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    })
  })

  describe("[PUT /api/users/:id] update", function () {
    const logger = require('./../../lib/utils').logger

    let modelSaveStub
    let loggerInfoStube

    it("should save the model and respond with res.json()", sinon.test(function (done) {

      req.user = { id: 0 }
      req.form = { isValid: true, model: { id: 0, save: function () {} } }
      modelSaveStub = sinon.stub(req.form.model, 'save')
      modelSaveStub.resolves({})
      loggerInfoStube = sinon.stub(logger, 'info')
      json = res.json = sinon.spy()
      
      co.wrap(user.update)(req, res, next).then(() => {
        expect(modelSaveStub.calledOnce).to.equal(true)
        expect(modelSaveStub.calledBefore(json)).to.equal(true)
        expect(loggerInfoStube.calledOnce).to.equal(true)
        expect(loggerInfoStube.calledBefore(json)).to.equal(true)
        expect(json.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    }))

    it("should respond with next() if form is not valid", function (done) {

      req.user = { id: 0 }
      req.form = { isValid: false, model: { id: 0, save: function () {} } }
      
      co.wrap(user.update)(req, res, next).then(() => {
        expect(next.calledOnce).to.equal(true)

        done()
      })
      .catch(e => done(e))
    })

    it("should respond with next() if the requester id is not the target id", function (done) {

      req.user = { id: 0 }
      req.form = { isValid: true, model: { id: 4, save: function () {} } }
      
      co.wrap(user.update)(req, res, next).then(() => {
        expect(next.calledOnce).to.equal(true)

        done()
      })
      .catch(e => done(e))
    })
  })

  describe("[DELETE /api/users/:id] delete", function () {
    const logger = require('./../../lib/utils').logger

    let loggerInfoStube

    it("should get and remove the user and respond with res.json()", sinon.test(function (done) {
      
      req.user = { id: 0 }
      req.params = { id: 0 }
      mockUser.expects('findByIdAndRemove').resolves({})
      loggerInfoStube = sinon.stub(logger, 'info')
      json = res.json = sinon.spy()
      
      co.wrap(user.delete)(req, res, next).then(() => {
        mockUser.verify()
        expect(loggerInfoStube.calledOnce).to.equal(true)
        expect(loggerInfoStube.calledBefore(json)).to.equal(true)
        expect(json.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    }))

    it("should respond with next if the requester id is not the target id", function (done) {
      
      req.user = { id: 0 }
      req.params = { id: 4 }
      
      co.wrap(user.delete)(req, res, next).then(() => {
        expect(next.calledOnce).to.equal(true)

        done()
      })
      .catch(e => done(e))
    })

    it("should try getting the user and respond with next if no user found", function (done) {
      
      req.user = { id: 0 }
      req.params = { id: 0 }
      mockUser.expects('findByIdAndRemove').resolves(null)
      
      co.wrap(user.delete)(req, res, next).then(() => {
        mockUser.verify()
        expect(next.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    })
  })

  describe("[POST /api/signin] signin", function () {
    const logger = require('./../../lib/utils').logger
    const bcrypt = require('bcrypt')
    const security = require('./../../lib/utils').security

    let loggerInfoStube
    let bcryptCompareStub
    let securitySignJwtStub

    it("should check given credentials, generate the token and respond with res.json()", sinon.test(function (done) {

      req.form = { isValid: true, model: { id: 0, save: function () {} } }
      mockUser.expects('findOne').resolves({})
      bcryptCompareStub = sinon.stub(bcrypt, 'compare')
      bcryptCompareStub.resolves(true)
      securitySignJwtStub = sinon.stub(security, 'signJwt')
      securitySignJwtStub.resolves({})
      loggerInfoStube = sinon.stub(logger, 'info')
      json = res.json = sinon.spy()
      
      co.wrap(user.signin)(req, res, next).then(() => {
        mockUser.verify()

        expect(bcryptCompareStub.calledOnce).to.equal(true)
        expect(bcryptCompareStub.calledBefore(securitySignJwtStub)).to.equal(true)

        expect(securitySignJwtStub.calledOnce).to.equal(true)
        expect(securitySignJwtStub.calledBefore(loggerInfoStube)).to.equal(true)

        expect(loggerInfoStube.calledOnce).to.equal(true)
        expect(loggerInfoStube.calledBefore(json)).to.equal(true)

        expect(json.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    }))

    it("should called next() if form is not valid", function (done) {

      req.form = { isValid: false, model: { id: 0, save: function () {} } }
      
      co.wrap(user.signin)(req, res, next).then(() => {
        expect(next.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    })

    it("should called next() if no user found with the username given", function (done) {

      req.form = { isValid: true, model: { id: 0, save: function () {} } }
      mockUser.expects('findOne').resolves(null)
      
      co.wrap(user.signin)(req, res, next).then(() => {
        mockUser.verify()
        expect(next.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    })

    it("should called next() if credentials are not valid", function (done) {

      req.form = { isValid: true, model: { id: 0, save: function () {} } }
      mockUser.expects('findOne').resolves({})
      bcryptCompareStub = sinon.stub(bcrypt, 'compare')
      bcryptCompareStub.resolves(false)
      
      co.wrap(user.signin)(req, res, next).then(() => {
        mockUser.verify()

        expect(bcryptCompareStub.calledOnce).to.equal(true)
        expect(bcryptCompareStub.calledBefore(next)).to.equal(true)

        expect(next.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    })
  })

  describe("[POST /api/signup] signup", function () {
    const logger = require('./../../lib/utils').logger
    const bcrypt = require('bcrypt')

    let modelSaveStub
    let loggerInfoStube
    let bcryptHashStub

    it("should check given credentials, generate the token and respond with res.json()", sinon.test(function (done) {

      req.form = { isValid: true, model: { id: 0, save: function () {} } }
      modelSaveStub = sinon.stub(req.form.model, 'save')
      modelSaveStub.resolves({})
      bcryptHashStub = sinon.stub(bcrypt, 'hash')
      bcryptHashStub.resolves('')
      loggerInfoStube = sinon.stub(logger, 'info')
      json = res.json = sinon.spy()
      
      co.wrap(user.signup)(req, res, next).then(() => {
        expect(bcryptHashStub.calledOnce).to.equal(true)
        expect(bcryptHashStub.calledBefore(loggerInfoStube)).to.equal(true)

        expect(loggerInfoStube.calledOnce).to.equal(true)
        expect(loggerInfoStube.calledBefore(json)).to.equal(true)

        expect(json.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    }))

    it("should called next() if form is not valid", function (done) {

      req.form = { isValid: false, model: { id: 0, save: function () {} } }
      
      co.wrap(user.signup)(req, res, next).then(() => {
        expect(next.calledOnce).to.equal(true)

        done()
      })
      .catch(e => {
        done(e)
      })
    })
  })
})
