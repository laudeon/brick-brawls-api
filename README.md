# Getting started

* run `npm install`
* run your mongod db service
* Edit the .env-dist file and rename it as .env
* run `npm run start` or using nodemon `nodemon`

# Tests

run all tests with `npm run test`