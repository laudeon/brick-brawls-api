/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const http = require('http')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

if(process.env.NODE_ENV !== 'production') {
  // Load configurations in process env before load app(s)
  dotenv.config()
}

const app = require('./lib')

// Mongodb connection
mongoose.connect(
  'mongodb://' +
  process.env.DB_USER +
  ':' +
  process.env.DB_PWD +
  '@' +
  process.env.DB_HOST +
  ':' +
  process.env.DB_PORT +
  '/' +
  process.env.DB_NAME,

  {
    config:
    {
      autoIndex: false,
    },
  }
)

// Get port from environment and store in Express.
const port = process.env.PORT || '3000'

// Create HTTP server.
const server = http.createServer(app)

server.listen(port)
