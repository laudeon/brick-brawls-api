/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const User = require('./../models/user')
const brawl = require('./../brawls/brawl')
const utils = require('./../utils')
const logger = utils.logger

// GET '/api/brawl/trial' handler
module.exports.trial = function* (req, res, next) {

}

// GET '/api/brawl/:enemy' handler
module.exports.brawl = function* (req, res, next) {
	let user
	let enemy
	let brawlRes
	let looserNumPlayer
	let winnerNumPlayer
	let lastAction

	user = yield User.findById(req.user.id)
	enemy = yield User.findById(req.params.enemy)

	try {
		brawlRes = brawl.resolve([user, enemy])
	} catch(e) {
		logger.error(e)
		next(e)
	}

	lastAction = brawl.getLastAction(brawlRes)

	// Someone win/loose
	if (lastAction.action == 'loose') {
		looserNumPlayer = lastAction.player
		winnerNumPlayer = looserNumPlayer === 0 ? 1 : 0
		brawlRes.summary = {}
		brawlRes.summary.winner = winnerNumPlayer
		brawlRes.summary.looser = looserNumPlayer
		if(looserNumPlayer == 1) {
			user.brick.wins = user.brick.wins + 1
			user.brick.experience = user.brick.experience + 20
			enemy.brick.experience = enemy.brick.experience + 10
		} else {
			enemy.brick.wins = enemy.brick.wins + 1
			enemy.brick.experience = enemy.brick.experience + 20
			user.brick.experience = user.brick.experience + 10
		}
	// tie game	
	} else {
		user.brick.experience = user.brick.experience + 10
		enemy.brick.experience = enemy.brick.experience + 10
	}

	user.brawls.push(brawlRes)
	user.brick.total = user.brick.total + 1
	yield user.save()

	enemy.brawls.push(brawlRes)
	enemy.brick.total = enemy.brick.total + 1
	yield enemy.save()

	res.json(brawlRes)
}
