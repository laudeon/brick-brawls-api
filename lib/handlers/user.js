/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const User = require('./../models/user')
const utils = require('./../utils')
const bcrypt = require('bcrypt')
const logger = utils.logger
const security = utils.security

// POST '/users/signin' handler
module.exports.signin = function* (req, res, next) {
  let token
  let user
  let validCredentials

  if(!req.form.isValid) {
    return next({ status: 400, message: req.form.errors })
  }

  user = yield User
                  .findOne({ username: req.form.model.username })
                  .select('username email ai brick vmoney password')
  if(!user) {
    return next({ status: 401, message: ['bad credentials'] })
  }

  validCredentials = yield bcrypt.compare(req.form.model.password, user.password)
  if(!validCredentials) {
    return next({ status: 401, message: ['bad credentials'] })
  }

  token = yield security.signJwt(user) // Exp in 3h

  delete user.password
  logger.info('user logged in', { user_id: user.id })
  res.json({ status: 200, token: token, expIn: security.jwtExp, user: user })
}

// POST '/users/signup' handler
module.exports.signup = function* (req, res, next) {
  let token
  let user

  if(!req.form.isValid) {
    return next({ status: 400, message: req.form.errors })
  }

  user = req.form.model
  user.password = yield bcrypt.hash(req.form.model.password, 10)
  yield user.save()

  logger.info('user created', { user_id: user.id })
  res.json({ status: 201 })
}

// GET '/api/users' handler
module.exports.list = function* (req, res, next) {
  let users

  users = yield User
                  .find()
                  .select('username email _id brick')

  res.json({ status: 200, users: users })
}

// GET '/api/users/:id' handler
module.exports.show = function* (req, res, next) {
  let user

  user = yield User
                .findById(req.user.id)
                .select('username email brick')

  if(!user) {
    return next({ status: 404, message: ['no user found for the given id'] })
  }

  res.json({ status: 200, user: user })
}

// PUT '/api/users/:id' handler
module.exports.update = function* (req, res, next) {
  if(!req.form.isValid) {
    return next({ status: 400, message: req.form.errors })
  }
  if(req.form.model.id !== req.user.id) {
    return next({ status: 403, message: ['unauthorized'] })
  }

  yield req.form.model.save()

  logger.info('user updated', { id: req.form.model.id })
  res.json({ status: 201, user: req.form.model })
}

// DELETE '/api/users/:id' handler
module.exports.delete = function* (req, res, next) {
  let user

  if(req.params.id !== req.user.id) {
    return next({ status: 403, message: ['unauthorized'] })
  }

  user = yield User.findByIdAndRemove(req.params.id)

  if(!user) {
    return next({ status: 404, message: ['no user found for the given id'] })
  }

  logger.info('user deleted', { user_id: user.id })
  res.json({ status: 204 })
}
