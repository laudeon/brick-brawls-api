/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const Weapons = require('./../models/weapons')
const Armors = require('./../models/weapons')
const utils = require('./../utils')
const logger = utils.logger

// GET '/api/store' handler
module.exports.list = function* (req, res, next) {
	let weapons
	let armors

	weapons = Weapons.list
	armors = Armors.list

	res.json({ status: 200, weapons: weapons, armors: armors })
}

module.exports.buy = function* (req, res, next) {

}

module.exports.sell = function* (req, res, next) {
  
}
