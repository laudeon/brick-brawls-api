/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const errors = require('./errors')
const cors = require('./cors')

module.exports = {
	errors: errors,
	cors: cors
}
