/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const logger = require('./../utils').logger

module.exports = {

  // Catch 404 and forward to error handler
  notFound: function (req, res, next) {
    let err = {}
    err.status = 404

    next(err)
  },

  // Development error handler
  // will print stacktrace
  serverDev: function (err, req, res, next) {
    err.request = req.originalUrl
    err.method = req.method
    logger.error(err.message, err)

    if (err.code === 11000) {
      return res
        .status(400)
        .json({ status: 400, message: err.message })
    }

    res
      .status(err.status || 500)
      .json({
        status: err.status,
        message: err.message,
        error: err
      })
  },

  // Production error handler
  // no stacktraces leaked to user
  server: function (err, req, res, next) {
    err.request = req.originalUrl
    err.method = req.method
    logger.error(err.message, err)

    if (err.code === 11000) {
      return res
        .status(400)
        .json({ status: 400, message: 'duplicate entry error' })
    }

    res
      .status(err.status || 500)
      .json({
        status: err.status,
        message: err.message
      })
  }
}
