/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

let roundLogs
let round

// Set the current round number
module.exports.setRound = function (roundNumber = 0) {
  round = roundNumber
}

// Force the roundLogs value
// @param {Array} actions
module.exports.setRoundLogs = function (logs = []) {
  roundLogs = logs
}
// Push a brick round action into the roundLogs array
// @param {Object} brick
// @param {String} action
// @returns {Number} the length of roundLogs
module.exports.addRoundLog = function (brick, action) {
  let logObject = {}

  logObject.round = round
  logObject.player = brick.playerNum
  logObject.lifepoints = brick.lifepoints
  logObject.mouvementpoints = brick.mouvementpoints
  logObject.actionpoints = brick.actionpoints
  logObject.position = brick.position
  logObject.action = action

  return roundLogs.push(logObject)
}
// @returns {Array} roundLogs
module.exports.getRoundLogs = () => roundLogs
