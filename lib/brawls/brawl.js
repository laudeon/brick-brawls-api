/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const brick = require('./brick')
const brawlLogger = require('./logger')

let brawl = []
let maxRounds = 30
let gridSize = 18
let bricks = []


// Add a brick for a brawl
// This method should be called twice (Two bricks for a brawl)
// @param {Object} the user object
// @returns {Integer} the current number of brick registered for the brawl
let addBrick = function (givenUser) {
	let brickLength = bricks.length

	if(brickLength >= 2) {
		return false
	}
	if(typeof givenUser !== 'object' || typeof givenUser.brick !== 'object') {
		return false
	}

	// Setup brick position
	if(brickLength === 0) {
		givenUser.brick.position = [0, 0]
	} else if(brickLength === 1) {
		givenUser.brick.position = [gridSize, gridSize]
	}

	// TODO calc velocity for who start ?!
	givenUser.brick.playerNum = brickLength
	givenUser.brick.code = givenUser.ai.js

	return bricks.push(givenUser.brick)
}

// Update brick info into the brick array with the given brick object and array index
// @param {Object} brick object
// @param {Integer} index of the bricks array to update
// @returns void
let updateBrick = function (brick, index) {
	bricks[index] = brick
}

// Update the given brick enemy into the bricks array
// @param {Object} Brick
// @param {Integer} index
// @returns void
let updateBrickEnemy = function (brick, index) {
	if(bricks[(index + 1)]) {
		bricks[(index + 1)] = brick.enemy
		//Needed because of avoiding circular JSON reference
		bricks[(index + 1)].enemy = brick
	}
	else if(bricks[(index - 1)]) {
		bricks[(index - 1)] = brick.enemy
		//Needed because of avoiding circular JSON reference
		bricks[(index - 1)].enemy = brick
	}
}

// Resolves a brawl, returns the result in an array which contains each round result
// @param Array bricksObject
// @return Array
module.exports.resolve = function (users) {
	let round = 0
	let winner = false
	let brick0origin
	let brick1origin

	// Init bricks array
	users.forEach((currentUser) => addBrick(currentUser))

	// Prevents JSON circular reference
	brick0origin = bricks[0]
	brick1origin = bricks[1]
	bricks[0].enemy = brick1origin
	bricks[1].enemy = brick0origin

	for(round ; round < maxRounds ; round++) {
		brawlLogger.setRoundLogs([])
		brawlLogger.setRound(round)

		// For each brick 
		// Init action points and mouvement points
		// run the brick's AI 
		// update brick infos and brick enemy info
		// Add round action into the roundActions array
		// If enemy is died, end the brawl
		bricks.every((currentBrick, index) => {
			// Reset ap and mp
			currentBrick.actionpoints = 5
			currentBrick.mouvementpoints = 3
			brick.setBrick(currentBrick)

			// Execute brick AI
			try {
				eval(currentBrick.code)
			} catch(e) {
				console.log(e)
			}

			// Update brick values into the bricks array for the next loop turn
			currentBrick = brick.getBrick()
			updateBrick(currentBrick, index)

			// If enemy is dead at the end of the brick round
			if(currentBrick.enemy.lifepoints <= 0) {
				brawlLogger.addRoundLog(currentBrick, 'loose')
				winner = true

				return false // Little trick to break the every 'loop'
			}

			// Update bricks array with new each brick caracteristic values
			updateBrickEnemy(currentBrick, index)
			brawlLogger.addRoundLog(currentBrick, 'end brick round')

			return true // Little trick to break the every 'loop'
		})

		brawl.push(brawlLogger.getRoundLogs())
		console.log(brawlLogger.getRoundLogs())
		console.log(brawl)

		if(winner) {
			break
		}
	}

	return brawl
}

// Retrive the last brawl actions (who loose? tie game?)
// @returns Object
module.exports.getLastAction = function (brawlRes) {
	return brawlRes[brawlRes.length - 1][brawlRes[brawlRes.length - 1].length - 1]
}
