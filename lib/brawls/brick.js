/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const armors = require('../models/armors')
const weapons = require('../models/weapons')
const brawlLogger = require('./logger')

let brick

// Private methods

// Retrieve the x and y brick's enemy distance
// @returns {Array} an array with the [x, y] distance values
let getEnemyDistance = function() {
	let distances = []
	distances.push(Math.abs(brick.position[0] - brick.enemy.position[0]))
	distances.push(Math.abs(brick.position[1] - brick.enemy.position[1]))

	return distances
}

// Determines in which direction the brick must move
// @returns {String} the direction 'x' or 'y'
let determineMovingDirection = function (enemyDistance, reverse = false) {
	let direction = 'y'

	// X or Y?
	if(enemyDistance[0] > enemyDistance[1]) {
		direction = 'x'
	} else if (enemyDistance[0] < enemyDistance[1]) {
		direction = 'y'
	}

	if(reverse) {
		direction = direction === 'x' ? 'y' : 'x'
	}

	return direction
}

// Determines the sign in order to increment or decrement the brick (x or y) position
// @returns {String} '+' or '-'
let determineMovingSign = function (direction, brick, reverse = false) {
	let sign = '+'

	// + or -?
	if(direction === 'x') {
		if(brick.position[0] > brick.enemy.position[0]) {
			sign = '-'
		} else if (brick.position[0] < brick.enemy.position[0]) {
			sign = '+'
		} else {
			sign = null
		}
	} else if(direction === 'y') {
		if(brick.position[1] > brick.enemy.position[1]) {
			sign = '-'
		} else if (brick.position[1] < brick.enemy.position[1]) {
			sign = '+'
		} else {
			sign = null
		}
	}

	if(reverse) {
		sign = sign === '+' ? '-' : '+'
	}

	return sign
}

// Modify the brick position datas forward its enemy (one case, one mv point)
let moveForward = function () {
	let direction = determineMovingDirection(getEnemyDistance(brick))
	let sign = determineMovingSign(direction, brick)

	applyBrickPosition(sign, direction)
}

// Modify the brick position datas backward its enemy (one case, one mv point)
let moveBackward = function() {
	let direction = determineMovingDirection(getEnemyDistance(brick), true)
	let sign = determineMovingSign(direction, brick, true)

	applyBrickPosition(sign, direction)
}

// Modify the brick position datas according to the given sign and direction
// @param {String} sign
// @param {String} direction
let applyBrickPosition = function (sign, direction) {
	let borderXMin = false
	let borderXMax = false
	let borderYMin = false
	let borderYMax = false

	// At the border(s)?
	if(brick.position[0] === 0) {
		borderXMin = true
	} else if(brick.position[0] === 18) {
		borderXMax = true
	}
	if(brick.position[1] === 0) {
		borderYMin = true
	} else if(brick.position[1] === 18) {
		borderYMax = true
	}

	// Move x
	if(direction === 'x' && sign === '-' && !borderXMin) {
		brick.position = [brick.position[0] - 1, brick.position[1]]
	} else if(direction === 'x' && sign === '+' && !borderXMax) {
		brick.position = [brick.position[0] + 1, brick.position[1]]
	} else if (direction === 'x' && sign === '-' && borderXMin && !borderYMin) {
		brick.position = [brick.position[0], brick.position[1] - 1]
	} else if (direction === 'x' && sign === '+' && borderXMax && !borderYMax) {
		brick.position = [brick.position[0], brick.position[1] + 1]
	}
	// Move y
	if(direction === 'y' && sign === '-' && !borderYMin) {
		brick.position = [brick.position[0], brick.position[1] - 1]
	} else if(direction === 'y' && sign === '+' && !borderYMax) {
		brick.position = [brick.position[0], brick.position[1] + 1]
	} else if (direction === 'y' && sign === '-' && borderYMin && !borderXMin) {
		brick.position = [brick.position[0] - 1, brick.position[1]]
	} else if (direction === 'y' && sign === '+' && borderYMax && !borderXMax) {
		brick.position = [brick.position[0] + 1, brick.position[1]]
	}
}

// Test if the brick can shot on its enemy (distance and action points test)
// @returns {Boolean}
let canShot = function () {
	let enemyDistance
	let weapon

	if(!brick.weapon) {
		return false
	}

	enemyDistance = getEnemyDistance(brick)
	weapon = brick.weapon

	return (enemyDistance[0] <= weapon.range || enemyDistance[1] <= weapon.range) && brick.actionpoints >= weapon.apCost
}

// Public API

module.exports.setBrick = givenBrick => brick = givenBrick
module.exports.getBrick = () => brick

// Move a brick
// params {String} to must be 'forward' OR 'backward'
module.exports.move = function (to) {
	switch(to) {
		case 'forward':
			moveForward(brick)
			brick.mouvementpoints = brick.mouvementpoints - 1
			brawlLogger.addRoundLog(brick, 'move forward enemy')
			break
		case 'backward':
			moveBackward(brick)
			brick.mouvementpoints = brick.mouvementpoints - 1
			brawlLogger.addRoundLog(brick, 'move backward enemy')
			break
		default:
			return null
	}
}

// Brick use its weapon against its enemy
// @returns {Integer} the quantity of dammages or 0 if something went wrong
module.exports.shot = function () {
	let dammages

	if(canShot()) {
		brick.actionpoints = brick.actionpoints - brick.weapon.apCost
		brick.enemy.armor = brick.enemy.armor || {}
		brick.enemy.armor.def = brick.enemy.armor.def || 0 // prevent not armor
		dammages = brick.strength + brick.weapon.deg - ((brick.enemy.dexterity + brick.enemy.armor.def) / 2)
		brick.enemy.lifepoints = brick.enemy.lifepoints - dammages

		brawlLogger.addRoundLog(brick, 'shoot enemy. ' + dammages + ' dammages')

		return dammages
	} else {
		brawlLogger.addRoundLog(brick, 'cannot shot')
		return 0
	}
}

// Test if the brick can shot on its enemy (distance and action points test)
// @returns boolean
module.exports.canShot = canShot

module.exports.hasWeapon = () => brick.weapon ? true : false
module.exports.hasArmor = () => brick.armor ? true : false

// Equip a brick with the given weapon if all requirements are ok (action points, exist)
// @param {String} the weapon name
module.exports.getWeapon = function (givenWeapon) {
	let weapon = weapons.findOneByName(givenWeapon)
	
	if(!weapon) {
		brawlLogger.addRoundLog(brick, 'no weapon found for the given name: ' + givenWeapon)
		return null
	}

	if(brick.actionpoints >= 1) {
		brick.weapon = weapon
		brick.actionpoints = brick.actionpoints - 1
		brawlLogger.addRoundLog(brick, 'gets weapon: ' + givenWeapon)
	} else {
		brawlLogger.addRoundLog(brick, 'not enough action points to get weapon')
	}

}

// Equip a brick with the given armor if all requirements are ok (action points, exist)
// @param {String} the armor name
module.exports.getArmor = function (givenArmor) {
	let armor = armors.findOneByName(givenArmor)

	if(!armor) {
		brawlLogger.addRoundLog(brick, 'no armor found for the given name: ' + givenWeapon)
		return null
	}

	if(brick.actionpoints >= 1) {
		brick.armor = armor
		brick.actionpoints = brick.actionpoints - 1
	} else {
		brawlLogger.addRoundLog(brick, 'not enough action points to get armor')
	}

}

module.exports.getActionPoints = () => brick.actionpoints
module.exports.getMouvementPoints = () => brick.mouvementpoints
