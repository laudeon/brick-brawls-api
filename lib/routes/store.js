/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const express = require('express')
const router = express.Router()
const storeHandler = require('./../handlers/store')
const wrap = require('co-express')

module.exports = function() {

	router.get('/api/store', wrap(storeHandler.list))

	return router
}
