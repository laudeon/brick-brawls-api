/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const user = require('./user')
const store = require('./store')
const brawl = require('./brawl')
const passport = require('passport')

module.exports.bootstrap = function (app) {

	app.use(
		'/api', 
		function (req, res, next) {
			if(req.method === 'OPTIONS') {
				next()
			}

			passport.authenticate('jwt', { session: false })(req, res, next)
		}
	)
	app.use(store())
	app.use(user())
	app.use(brawl())
}
