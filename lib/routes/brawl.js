/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const express = require('express')
const router = express.Router()
const brawlHandler = require('./../handlers/brawl')
const wrap = require('co-express')

module.exports = function() {

	router.get('/api/brawl/trial', wrap(brawlHandler.trial))
	router.get('/api/brawl/:enemy', wrap(brawlHandler.brawl))

	return router
}
