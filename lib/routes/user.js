/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const express = require('express')
const router = express.Router()
const userHandler = require('./../handlers/user')
const wrap = require('co-express')
const signinForm = require('./../forms/signin')
const signupForm = require('./../forms/signup')
const updateForm = require('./../forms/update')

module.exports = function() {

	router.get('/api/users', wrap(userHandler.list))
	router.get('/api/users/:id', wrap(userHandler.show))

	router.post('/users/signup', signupForm.process, wrap(userHandler.signup))
	router.post('/users/signin', signinForm.process, wrap(userHandler.signin))

	router.put('/api/users/:id', updateForm.process, wrap(userHandler.update))

	router.delete('/api/users/:id', wrap(userHandler.delete))

	return router
}
