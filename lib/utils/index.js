/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const passport = require('./passport')
const logger = require('./logger')
const security = require('./security')

module.exports = {
  logger: logger,
  passport: passport,
  security: security
}