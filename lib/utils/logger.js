/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const winston = require('winston')
const path = require('path')
 
winston.add(
  winston.transports.File, {
    name: 'info',
    filename: path.join(__dirname, '..', '..', 'log', 'info.log'),
    level: 'info',
    json: true,
    eol: '\n', // for Windows, or `eol: ‘n’,` for *NIX OSs
    timestamp: true
  }
)

winston.add(
  winston.transports.File, {
    name: 'error',
    filename: path.join(__dirname, '..', '..', 'log', 'error.log'),
    level: 'error',
    json: true,
    eol: '\n',
    timestamp: true,
    handleExceptions: true,
    humanReadableUnhandledException: true
  }
)

winston.add(
  winston.transports.File, {
    name: 'debug',
    filename: path.join(__dirname, '..', '..', 'log', 'debug.log'),
    level: 'debug',
    json: true,
    eol: '\n',
    timestamp: true
  }
)

winston.level = process.env.LOG_LEVEL

module.exports = winston
