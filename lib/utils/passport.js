/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const User = require('./../models/user')
const passport = require('passport')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt

let options = {}
options.jwtFromRequest = ExtractJwt.fromAuthHeader()
options.secretOrKey = process.env.SECRET_JWT

passport.use(new JwtStrategy(options, (jwt_payload, done) => {
  User
    .findById(jwt_payload.sub)
    .then(user => done(null, user))
    .catch(error => done(error))
}))

module.exports = passport
