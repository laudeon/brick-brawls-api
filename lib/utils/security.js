/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const bluebird = require('bluebird')
const jwt = bluebird.promisifyAll(require('jsonwebtoken'))

let jwtExp = 60 * 180 // 3h

module.exports.signJwt =
  user => jwt.signAsync({ sub: user.id }, process.env.SECRET_JWT, { expiresIn: jwtExp})

module.exports.jwtExp = jwtExp
