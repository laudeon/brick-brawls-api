/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const middlewares = require('./middlewares')
const utils = require('./utils')
const routes = require('./routes')
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(middlewares.cors)
app.use(utils.passport.initialize())

routes.bootstrap(app)

// Errors handler
app.use(middlewares.errors.notFound)
if ('development' === app.get('env')) {
  app.use(middlewares.errors.serverDev)
} else {
  app.use(middlewares.errors.server)
}

module.exports = app
