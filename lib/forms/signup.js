/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const formHandler = require('express-form-handler')
const User = require('./../models/user')
const signin = require('./signin')
 
module.exports = formHandler.create([
  {
    name: 'email',
    label: 'Email',
    format: formHandler.format.email(),
    rules: formHandler.rule.required()
  },
  {
    name: 'passwordConfirm',
    label: 'Confirm password',
    format: formHandler.format.string(),
    rules: [
      formHandler.rule.required(),
      formHandler.rule.equalsto('password')
    ]
  },
  {
    name: 'brick',
    label: 'Brick name',
    format: formHandler.format.string(),
    rules: formHandler.rule.required()
  }
])
.config({
  modelStrategy: new formHandler.MongooseStrategy(User),
  validationByModel: false
})
.extends(signin)
