/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const formHandler = require('express-form-handler')
const User = require('./../models/user')
 
module.exports = formHandler.create([
  {
    name: 'username',
    label: 'Username',
    format: formHandler.format.string(),
    rules: [
      formHandler.rule.required(), 
      formHandler.rule.minlength(4)
    ]
  }, 
  {
    name: 'password',
    label: 'Password',
    format: formHandler.format.string(),
    rules: [
      formHandler.rule.required(), 
      formHandler.rule.minlength(6)
    ]
  }
])
.config({
  modelStrategy: new formHandler.MongooseStrategy(User),
  validationByModel: false
})
