/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const formHandler = require('express-form-handler')
const User = require('./../models/user')

module.exports = formHandler.create([
  {
    name: 'username',
    label: 'Username',
    format: formHandler.format.string(),
    rules: 
    [
      formHandler.rule.required(),
      formHandler.rule.minlength(4)
    ]
  },
  {
    name: 'email',
    label: 'Email',
    format: formHandler.format.email(),
    rules: formHandler.rule.required()
  },
  {
    name: 'brick',
    label: 'Brick',
    format: formHandler.format.string() // TODO create custom format brick
  },
  {
    name: 'ai',
    label: 'Ai',
    format: formHandler.format.string()
  }
])
.config({
  modelStrategy: new formHandler.MongooseStrategy(User),
  validationByModel: false
})
