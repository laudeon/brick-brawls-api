/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const mongoose = require('mongoose')
mongoose.Promise = Promise
const BrickSchema = new mongoose.Schema({
        name: { type: String, default: 'myBrick' }, 
        level: { type: Number, default: 1 },
        experience: { type: Number, default: 0 },
        wins: { type: Number, default: 0 },
        total: { type: Number, default: 0 },
        strength: { type: Number, default: 10 },
        dexterity: { type: Number, default: 10 },
        lifepoints: { type: Number, default: 100 },
        actionpoints: { type: Number, default: 5 },
        mouvementpoints: { type: Number, default: 3 },
        weapons: { type: Array, default: [
          { name: 'gun', apCost: 2, deg: 10, precision: 0.95, range: 3 }
        ] },
        armors: { type: Array, default: [
          { name: 'helmet', apCost: 1, def: 10 }
        ] }
      })

module.exports = BrickSchema
