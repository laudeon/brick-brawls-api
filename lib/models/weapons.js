/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

let weapons = [
	{ name: 'gun', apCost: 2, deg: 10, precision: 0.95, range: 3 },
	{ name: 'ak-47', apCost: 3, deg: 18, precision: 0.75, range: 5 }
]

module.exports.findOneByName = function(name) {
	return weapons.find(weapon => weapon.name === name)
}

module.exports.list = weapons
