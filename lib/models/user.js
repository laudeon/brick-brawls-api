/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

const mongoose = require('mongoose')
mongoose.Promise = Promise
const BrickSchema = require('./brick')
const UserInfoSchema = new mongoose.Schema({
        username: {
          type: String,
          trim: true,
          unique: true,
        },
        password: {
          type: String,
          trim: true,
        },
        email: {
          type: String,
          trim: true,
          unique: true,
        },
        brick: {
          type: BrickSchema,
          default: BrickSchema
        },
        ai: {
          type: Object,
          default: {
            js: '',
            xml: ''
          }
        },
        vmoney: {
          type: Number,
          default: 500
        },
        brawls: []
      },
      {
        collection: 'users',
        timestamps: true
      })

module.exports = mongoose.model('User', UserInfoSchema)
