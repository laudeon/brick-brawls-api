/*!
 * Brick Brawls API
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */
'use strict'

let armors = [
	{ name: 'helmet', apCost: 1, def: 10 },
	{ name: 'shield', apCost: 2, def: 15 }
]

module.exports.findOneByName = function(name) {
	return armors.find(armor => armor.name === name)
}

module.exports.list = armors
